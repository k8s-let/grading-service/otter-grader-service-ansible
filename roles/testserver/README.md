# Otter Grading Service Testing

This role is intended to install a integration test server for the *otter grading service*.
It installs a Docker compose environment consisting of

- a testing service submitting Jupyter notebooks (otter-grader-testing-in)
- otter-grader-pusher
- otter-grader-grader
- otter-grader-responder
- a testing service to evaluate the grading results (otter-grader-testing-out)


## Requirements

The host system must support Docker in daemon mode! The Unix socket controlling Docker will
be mounted into the grader service.

Free disk space depends on the image used for grading, which can be large. 60 GB recommended.

## Usage

Apply the role to a server. Launch the compose environment:

```
cd /root/otter-grader-testing/grader-test
./up.sh
```

Then check http://<hostname>/ (HTML) or http://<hostname>/status (JSON). 

The TLS certificates included are self-signed, the mockup CA is only used in this context.

## Results

The JSON returned looks like
```json
{
 "1":1,
 "2":1,
 "3":1,
 "summary":1,
 "version":"0.0.32.dev4+ge88c51c",
 "date":"2024-01-12 09:33:26",
 "timestamp":1705052006.1509154
}
```
It shows the results for 3 tests:
- submission without notebook (1 = missing notebook detected)
- submission with 1 notebook (1 = grading success)
- submission with 2 notebooks (1 = too many notebooks detected)

The summary is 1 if all tests have passed, 0 otherwise. Additionally, the version tested and the test date are included.

## Notes

Using the host Docker socket allows to make use of the host Docker cache. During installation,
the role initiates the cache by pulling the grader image, reducing the time to run the test
by minutes.

Don't use this approach for production environments! For permission reasons, the grading
process runs as root. This is acceptable for this controlled environment. In a production
environment, malicious notebooks could break out of the containment and access the host
system.
