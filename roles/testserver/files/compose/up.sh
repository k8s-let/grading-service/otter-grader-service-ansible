#!/bin/sh
docker pull registry.ethz.ch/k8s-let/grading-service/otter-grader-service:latest
docker pull registry.ethz.ch/k8s-let/grading-service/otter-grader-service:latest-testenv
docker pull registry.ethz.ch/k8s-let/grading-service/otter-grader-service-test-out:latest
docker pull registry.ethz.ch/k8s-let/grading-service/otter-grader-service-test-in:latest
docker-compose up -d
